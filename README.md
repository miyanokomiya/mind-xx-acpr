# mind-xx-acpr

> Mind XX ACPR project

## Build Setup

```bash
# install dependencies
yarn install

# serve with hot reload at localhost:8080
yarn dev

# build for production with minification
yarn build

# build & deploy to Firebase hosting
yarn build

# run unit tests
yarn unit

# run e2e tests
yarn e2e

# run all tests
yarn test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
