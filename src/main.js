// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import Vuetify from 'vuetify'
import './utils/windowState'
import './utils/isMobile'
import './utils/track'

import 'ress'
import 'vuetify/dist/vuetify.min.css'
import '@/assets/styles/icons.css'
import '@/assets/styles/main.scss'

Vue.use(Vuetify)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
